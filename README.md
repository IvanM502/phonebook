## Telefonski imenik

### Funkcionalnosti

Mogućnost dodavanja, brisanja i uređivanja kontakata.
Mogućnost dodavanja i brisanja kontakata iz bookmark sekcije.
Uz kontakt je moguće dodati hashtagove. Klikom na određeni hashtag prikazuju se svi kontakti s tim hashtagom.
Kontakt može imati jedan ili više brojeva, nula ili više emailova i hashtagova.
Mogućnost traženja kontakta prema imenu, prezimenu i hashtagu.

### Tehnologije
Na front-endu korišten AngularJS/Angular, a na back-endu ASP.NET Web Api tehnologija.
Podaci se spremaju u MS SQL Server uz korištenje entity frameworka.
