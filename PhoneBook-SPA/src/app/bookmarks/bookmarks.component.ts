import { Component, OnInit } from '@angular/core';
import { Contact } from '../_models/Contact';
import { ContactService } from '../_services/contact.service';
import { AlertifyService } from '../_services/alertify.service';
import {faBookmark} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-bookmarks',
  templateUrl: './bookmarks.component.html',
  styleUrls: ['./bookmarks.component.css']
})
export class BookmarksComponent implements OnInit {
  faBookmarkSolid = faBookmark;
  bookmarkedContacts: Contact[];

  constructor(private contactService: ContactService, private alertify: AlertifyService) { }

  ngOnInit() {
    this.contactService.getBookmarkedContacts().subscribe((contacts: Contact[]) => {
      this.bookmarkedContacts = contacts;
    }, error => console.log(error));
  }

  deleteContact(id: number) {
    this.alertify.confirm('Are you shure you want to delete this contact?', () => 
    this.contactService.deleteContact(id).subscribe(() => {
      this.bookmarkedContacts.splice(this.bookmarkedContacts.findIndex(i => i.contactId === id), 1);
      this.alertify.success('Contact has been deleted');
     }, error => {
       this.alertify.error('Failed to delete contact');
     }));
  }
  bookmarkCheck(id) {
     this.contactService.bookmarkContact(id).subscribe(() => {
      this.bookmarkedContacts.splice(this.bookmarkedContacts.findIndex(i => i.contactId === id), 1);
      this.alertify.success('Bookmark deleted');
    }, error => {this.alertify.error('Can not remove bookmark');
  }); 
  }

}
