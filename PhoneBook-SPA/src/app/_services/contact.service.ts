import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Contact } from '../_models/Contact';
import { SearchModel } from '../_models/SearchModel';

@Injectable({
  providedIn: 'root'
})
export class ContactService {
  baseUrl = environment.apiUrl;

constructor(private http: HttpClient) {}

getContact(id: number): Observable<Contact> {
  return this.http.get<Contact>(this.baseUrl + id);
}
getContacts(): Observable<Contact[]> {
  return this.http.get<Contact[]>(this.baseUrl);
}

addContact(model: Contact) {
  return this.http.post(this.baseUrl, model);
}

getBookmarkedContacts(): Observable<Contact[]> {
  return this.http.get<Contact[]>(this.baseUrl + 'bookmarks');
}

deleteContact(id: number) {
  return this.http.delete(this.baseUrl + id);
}

bookmarkContact(id: number) {
  return this.http.put(this.baseUrl + id + '/setbookmark', id);
}

updateContact(id: number, model: any) {
  return this.http.put(this.baseUrl + id, model);
}

searchContacts(model: SearchModel): Observable<Contact[]> {

  let params = new HttpParams();
  params = params.append('type', model.type);
  params = params.append('query', model.query);

  return this.http.get<Contact[]>(this.baseUrl + 'search', { params });
}

getContactsWithSameHashtag(name: string): Observable<Contact[]> {
  return this.http.get<Contact[]>(this.baseUrl + 'hashtag/' + name);
}

}
