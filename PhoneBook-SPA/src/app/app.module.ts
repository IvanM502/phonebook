import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { RouterModule } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { AppComponent } from './app.component';
import { NavComponent } from './nav/nav.component';
import { ContactAddComponent } from './contact-add/contact-add.component';
import { ContactService } from './_services/contact.service';
import { SearchComponent } from './search/search.component';
import { BookmarksComponent } from './bookmarks/bookmarks.component';
import { appRoutes } from './routes';
import { ContactEditComponent } from './contact-edit/contact-edit.component';
import { ContactHashtagsListComponent } from './contact-hashtags-list/contact-hashtags-list.component';



@NgModule({
   declarations: [
      AppComponent,
      NavComponent,
      ContactAddComponent,
      SearchComponent,
      BookmarksComponent,
      ContactEditComponent,
      ContactHashtagsListComponent
   ],
   imports: [
      BrowserModule,
      HttpClientModule,
      FormsModule,
      FontAwesomeModule,
      ReactiveFormsModule,
      RouterModule.forRoot(appRoutes),
      FontAwesomeModule
   ],
   providers: [
      ContactService
   ],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
