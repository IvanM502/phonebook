import { Component, OnInit, ViewChild } from '@angular/core';
import { ContactService } from '../_services/contact.service';
import { Contact } from '../_models/Contact';
import { FormGroup, FormBuilder, FormArray, Validators } from '@angular/forms';
import { AlertifyService } from '../_services/alertify.service';
import {faBookmark} from '@fortawesome/free-solid-svg-icons';
import {faBookmark as faBookmark2} from '@fortawesome/free-regular-svg-icons';

@Component({
  selector: 'app-contact-add',
  templateUrl: './contact-add.component.html',
  styleUrls: ['./contact-add.component.css']
})
export class ContactAddComponent implements OnInit {
  faBookmarkSolid = faBookmark;
  faBookmarkRegular = faBookmark2;
  newContactClick = false;
  contacts: Contact[];
  contactToAdd: Contact;

  nestedForm: FormGroup;

  constructor(private contactService: ContactService, private formBuilder: FormBuilder,
              private alertify: AlertifyService ) { }

  ngOnInit() {
    this.contactService.getContacts().subscribe((cont: Contact[]) => {
      this.contacts = cont;
    }, error => console.log(error));
  }

  newButtonContact() {
    this.newContactClick = !this.newContactClick;
    this.nestedForm = this.formBuilder.group({
    name: ['', Validators.required],
    surname: [''],
    adress: [''],
    bookmark: ['0'],
    numbers: this.formBuilder.array([this.addNumbersGroup()]),
    contactEmails: this.formBuilder.array([this.addEmailGroup()]),
    contactHashTags: this.formBuilder.array([this.addHashTagPostGroup()])
    });
  }
  // NUMBERS
  addNumbersGroup() {
    return this.formBuilder.group({
      number: ['', Validators.required]
    });
  }

  addNumber() {
    this.numbersArray.push(this.addNumbersGroup());
  }

  removeNumber(index) {
    this.numbersArray.removeAt(index);
  }

  get numbersArray() {
    return <FormArray> this.nestedForm.get('numbers');
  }

  // EMAILS
  addEmailGroup() {
    return this.formBuilder.group({
      email: ['']
    });
  }

  get emailsArray() {
    return <FormArray> this.nestedForm.get('contactEmails')
  }

  addEmail() {
    this.emailsArray.push(this.addEmailGroup());
  }

  removeEmail(index) {
    this.emailsArray.removeAt(index);
  }

  //hashTags
  addHashTagPostGroup() {
    return this.formBuilder.group({
      hashName: ['']
    });
  }

  get hashArray() {
    return <FormArray> this.nestedForm.get('contactHashTags')
  }

  addHashTag() {
    this.hashArray.push(this.addHashTagPostGroup());
  }

  removeHashTag(index) {
    this.hashArray.removeAt(index);
  }

 
  sub() {
    this.contactToAdd = this.nestedForm.value;
    console.log(this.contactToAdd);
  }

  submitContact() {
    this.contactToAdd = this.nestedForm.value;
    this.contactService.addContact(this.contactToAdd).subscribe((newCont: Contact) => {
      const contact: Contact = newCont;
      this.contacts.push(contact);
      this.alertify.success('Contact ' + contact.name + ' added.');
      this.nestedForm.reset({bookmark: 0});
    }, error => this.alertify.error('Failed to add new contact.'));
  }

  deleteContact(id: number) {
    this.alertify.confirm('Are you shure you want to delete this contact?', () =>
    this.contactService.deleteContact(id).subscribe(() => {
      this.contacts.splice(this.contacts.findIndex(i => i.contactId === id), 1);
      this.alertify.success('Contact has been deleted');
     }, error => {
       this.alertify.error('Failed to delete contact');
     }));
  }

  bookmarkCheck(id) {
    this.contactService.bookmarkContact(id).subscribe(() => {
      const cont = this.contacts.find(i => i.contactId === id);
      cont.bookmark = !cont.bookmark;
      if (cont.bookmark) {
        this.alertify.success('Contact added to bookmarks');
      } else if (!cont.bookmark) {
        this.alertify.success('Bookmar removed');
      }
    }, error => {this.alertify.error('Can not add bookmark');
  });
  }
}
