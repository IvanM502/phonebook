export interface SearchModel {
    type: string;
    query: string;
}
