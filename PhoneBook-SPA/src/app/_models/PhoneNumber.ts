
export interface PhoneNumber {
    phoneNumberId: number;
    number: string;
    contactId: number;
}
