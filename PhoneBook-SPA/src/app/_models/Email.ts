export interface Email {
    contactEmailId: number;
    email: string;
    contactId: number;
}
