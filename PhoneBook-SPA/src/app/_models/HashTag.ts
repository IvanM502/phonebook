export interface HashTag {
    hashTagId: number;
    hashName: string;
}
