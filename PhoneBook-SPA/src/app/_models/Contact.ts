import { PhoneNumber } from './PhoneNumber';
import { Email } from './Email';
import { HashTag } from './HashTag';

export interface Contact {
    contactId: number;
    name: string;
    surname: string;
    adress: string;
    bookmark: boolean;
    phoneNumbers: PhoneNumber[];
    emails: Email[];
    contactHashTags: HashTag[];
}
