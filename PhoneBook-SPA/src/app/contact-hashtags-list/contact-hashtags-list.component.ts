import { Component, OnInit } from '@angular/core';
import { ContactService } from '../_services/contact.service';
import { ActivatedRoute } from '@angular/router';
import { Contact } from '../_models/Contact';
import {faBookmark} from '@fortawesome/free-solid-svg-icons';
import {faBookmark as faBookmark2} from '@fortawesome/free-regular-svg-icons';

@Component({
  selector: 'app-contact-hashtags-list',
  templateUrl: './contact-hashtags-list.component.html',
  styleUrls: ['./contact-hashtags-list.component.css']
})
export class ContactHashtagsListComponent implements OnInit {
  faBookmarkSolid = faBookmark;
  faBookmarkRegular = faBookmark2;

  contacts: Contact[];

  constructor(private contactService: ContactService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.contactService.getContactsWithSameHashtag(this.route.snapshot.params.name).subscribe((cont: Contact[]) =>{
      this.contacts = cont;
    }, error => console.log(error));
  }

}
