import { Component, OnInit } from '@angular/core';
import {Location} from '@angular/common';
import { Contact } from '../_models/Contact';
import { ContactService } from '../_services/contact.service';
import { AlertifyService } from '../_services/alertify.service';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';

@Component({
  selector: 'app-contact-edit',
  templateUrl: './contact-edit.component.html',
  styleUrls: ['./contact-edit.component.css']
})
export class ContactEditComponent implements OnInit {
  contact: any;
  editForm: FormGroup;

  constructor(private contactService: ContactService, private alertify: AlertifyService,
              private route: ActivatedRoute, private formBuilder: FormBuilder, private location: Location) { }

  ngOnInit() {
    this.loadUser();
    this.newFormGroup();
   
  }

  loadUser() {
    this.contactService.getContact(this.route.snapshot.params.id).subscribe((contact: Contact) =>{
      this.contact = contact;
      this.editContact();
    }, error => console.log(error));
  }

  newFormGroup() {
    this.editForm = this.formBuilder.group({
      name: ['', Validators.required],
      surname: [''],
      adress: [''],
      bookmark: [''],
      numbers: this.formBuilder.array([this.addNumbersGroup()]),
      contactEmails: this.formBuilder.array([this.addEmailGroup()]),
      contactHashTags: this.formBuilder.array([this.addHashTagPostGroup()])
    });
  }

   // NUMBERS
  addNumbersGroup() {
    return this.formBuilder.group({
      number: ['', Validators.required]
    });
  }

  get numbersArray() {
    return this.editForm.get('numbers') as FormArray;
  }

  addNumber() {
    this.numbersArray.push(this.addNumbersGroup());
  }

  removeNumber(index) {
    this.numbersArray.removeAt(index);
  }

  // EMAILS
  addEmailGroup() {
    return this.formBuilder.group({
      email: ['']
    });
  }

  get emailsArray() {
    return this.editForm.get('contactEmails') as FormArray;
  }

  addEmail() {
    this.emailsArray.push(this.addEmailGroup());
  }

  removeEmail(index) {
    this.emailsArray.removeAt(index);
  }

  //hashTags
  addHashTagPostGroup() {
    return this.formBuilder.group({
      hashName: ['']
    });
  }

  get hashArray() {
    return <FormArray> this.editForm.get('contactHashTags')
  }

  addHashTag() {
    this.hashArray.push(this.addHashTagPostGroup());
  }

  removeHashTag(index) {
    this.hashArray.removeAt(index);
  }



  submitContact() {
    this.contactService.updateContact(this.route.snapshot.params.id, this.editForm.value).subscribe(() => {
      this.alertify.success('Contact updated');
    },error => this.alertify.error('Problem acoured douring update.')
    );
  }

  editContact() {
    this.editForm.patchValue( {
      name: this.contact.name,
      surname: this.contact.surname,
      adress: this.contact.adress,
      bookmark: this.contact.bookmark
    });

    for (let tag = 0; tag < this.contact.hashTags.length; tag++) {
      if (tag === 0) {
        this.hashArray.at(tag).patchValue({
          hashName: this.contact.hashTags[tag].hashTag.hashName
        });
      } else {
        const group = this.addHashTagPostGroup();
        group.patchValue({
          hashName: this.contact.hashTags[tag].hashTag.hashName
          });
        this.hashArray.push(group);
      }
    }


    for (let num = 0; num < this.contact.phoneNumbers.length; num++) {
      if (num === 0) {
        this.numbersArray.at(num).patchValue({
          number: this.contact.phoneNumbers[num].number
        });
      } else {
        const group = this.addNumbersGroup();
        group.patchValue({
            number: this.contact.phoneNumbers[num].number
          });
        this.numbersArray.push(group);
      }
    }

    for (let email = 0; email < this.contact.emails.length; email++) {
      if (email === 0) {
        this.emailsArray.at(email).patchValue({
          email: this.contact.emails[email].email
        });
      } else {
        const group = this.addEmailGroup();
        group.patchValue({
            email: this.contact.emails[email].email
          });
        this.emailsArray.push(group);
      }
    }
  }

  goBack() {
    this.location.back();
  }


}
