import { Component, OnInit, ViewChild } from '@angular/core';
import {faSearch} from '@fortawesome/free-solid-svg-icons';
import { SearchModel } from '../_models/SearchModel';
import { Contact } from '../_models/Contact';
import {faBookmark} from '@fortawesome/free-solid-svg-icons';
import {faBookmark as faBookmark2} from '@fortawesome/free-regular-svg-icons';
import { ContactService } from '../_services/contact.service';
import { AlertifyService } from '../_services/alertify.service';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  faSearch = faSearch;
  faBookmarkSolid = faBookmark;
  faBookmarkRegular = faBookmark2;
  searchModel: SearchModel;
  contacts: Contact[];
  searchForm: FormGroup;

  constructor(private contactService: ContactService, private alertify: AlertifyService,
              private formBuilder: FormBuilder ) { }

  ngOnInit() {
    this.searchForm = this.formBuilder.group({
      type: [''],
      query: ['']
    });

    this.onChange();
  }

  onChange() {
    this.searchForm.valueChanges.subscribe(val => {
      this.searchModel = this.searchForm.value;
      this.contactService.searchContacts(this.searchModel).subscribe((model: Contact[]) => {
        this.contacts = model;
      }, error => console.log(error));
    });
  }

  deleteContact(id: number) {
    this.alertify.confirm('Are you shure you want to delete this contact?', () =>
    this.contactService.deleteContact(id).subscribe(() => {
      this.contacts.splice(this.contacts.findIndex(i => i.contactId === id), 1);
      this.alertify.success('Contact has been deleted');
     }, error => {
       this.alertify.error('Failed to delete contact');
     }));
  }

  bookmarkCheck(id: number) {
    this.contactService.bookmarkContact(id).subscribe(() => {
      const cont = this.contacts.find(i => i.contactId === id);
      cont.bookmark = !cont.bookmark;
      if (cont.bookmark) {
        this.alertify.success('Contact added to bookmarks');
      } else if (!cont.bookmark) {
        this.alertify.success('Bookmar removed');
      }
    }, error => {this.alertify.error('Can not add bookmark');
  });
  }

}
