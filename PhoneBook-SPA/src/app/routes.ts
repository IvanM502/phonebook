import { Routes } from '@angular/router';
import { ContactAddComponent } from './contact-add/contact-add.component';
import { SearchComponent } from './search/search.component';
import { BookmarksComponent } from './bookmarks/bookmarks.component';
import { ContactEditComponent } from './contact-edit/contact-edit.component';
import { ContactHashtagsListComponent } from './contact-hashtags-list/contact-hashtags-list.component';



export const appRoutes: Routes = [ // fist match win sistem
   {path: 'contacts', component: ContactAddComponent},
   {path: 'contacts/:id', component: ContactEditComponent},
   {path: 'hashtags/:name', component: ContactHashtagsListComponent},
   {path: 'search', component: SearchComponent},
   {path: 'bookmarks', component: BookmarksComponent},
   {path: '**', redirectTo: 'contacts', pathMatch: 'full'}
   
];
