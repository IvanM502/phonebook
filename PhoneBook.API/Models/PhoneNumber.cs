namespace PhoneBook.API.Models
{
    public class PhoneNumber
    {
        public int PhoneNumberId { get; set; }
        public string Number { get; set; }
        public Contact Contact { get; set; }
        public int ContactId { get; set; }
    }
}