namespace PhoneBook.API.Models
{
    public class ContactEmail
    {
        public int ContactEmailId { get; set; }
        public string Email { get; set; }
        public Contact Contact { get; set; }
        public int ContactId { get; set; }
    }
}