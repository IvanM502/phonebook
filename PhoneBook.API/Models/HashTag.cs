using System.Collections.Generic;

namespace PhoneBook.API.Models
{
    public class HashTag
    {
        public int HashTagId { get; set; }
        public string HashName { get; set; }
        public ICollection<ContactHash> Contacts { get; set; }
    
    }
}