namespace PhoneBook.API.Models
{
    public class ContactHash
    {
        public int ContactId { get; set; }
        public int HashTagId { get; set; }
        public Contact Contact { get; set; }
        public HashTag HashTag { get; set; }
    }
}