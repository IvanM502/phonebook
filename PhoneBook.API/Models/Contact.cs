using System.Collections.Generic;

namespace PhoneBook.API.Models
{
    public class Contact
    {
        public int ContactId { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Adress { get; set; }
        public int Bookmark { get; set; }
        public ICollection<PhoneNumber> PhoneNumbers { get; set; }
        public ICollection<ContactEmail> Emails { get; set; }
        public ICollection<ContactHash> HashTags { get; set; }


    }
}