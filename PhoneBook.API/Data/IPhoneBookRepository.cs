using System.Collections.Generic;
using System.Threading.Tasks;
using PhoneBook.API.Dtos;
using PhoneBook.API.Models;

namespace PhoneBook.API.Data
{
    public interface IPhoneBookRepository
    {
         void Add<T> (T entity) where T: class;
         void Delete<T> (T entity) where T: class;
         void Update<T> (T entity) where T: class;
         Task<bool> SaveAll();
         Task<IEnumerable<Contact>> GetContacts();
         Task<Contact> GetContact(int id);
         Task<IEnumerable<Contact>> GetBookmarkedContacts();
         Task<ICollection<PhoneNumber>> GetPhoneNumbers(int id);
         Task<IEnumerable<Contact>> SearchContacts(SearchDto searchParams);
         Task<HashTag> GetHashTag(string hashName);
         Task<IEnumerable<Contact>> GetContactsWithSameTag(int hashTagId);

    }
}