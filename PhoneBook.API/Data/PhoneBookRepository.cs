using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PhoneBook.API.Dtos;
using PhoneBook.API.Models;

namespace PhoneBook.API.Data
{
    public class PhoneBookRepository : IPhoneBookRepository
    {
        private readonly DataContext _context;

        public PhoneBookRepository(DataContext context)
        {
            _context = context;

        }
        public void Add<T>(T entity) where T : class
        {
            _context.Add(entity);
        }

        public void Delete<T>(T entity) where T : class
        {
            _context.Remove(entity);
        }

        public void Update<T>(T entity) where T : class
        {
            _context.Update(entity);
        }

        public async Task<Contact> GetContact(int id)
        {
           var contact = await _context.Contacts.Include(n => n.PhoneNumbers).Include(e => e.Emails)
                    .Include(h => h.HashTags).ThenInclude(h => h.HashTag)
                    .FirstOrDefaultAsync(c => c.ContactId == id);

           return contact;
        }

        public async Task<IEnumerable<Contact>> GetContacts()
        {
             var contacts = await _context.Contacts.Include(n => n.PhoneNumbers).Include(e => e.Emails)
                        .Include(h => h.HashTags).ThenInclude(h => h.HashTag).ToListAsync();
            

           return contacts;
        }

        public async Task<bool> SaveAll()
        {
            return await _context.SaveChangesAsync() > 0;
        }
        
        public async Task<IEnumerable<Contact>> GetBookmarkedContacts()
        {
            var bookmrkedContacts = await _context.Contacts.Where(c => c.Bookmark == 1).Include(n => n.PhoneNumbers)
                .Include(e => e.Emails).Include(h => h.HashTags).ThenInclude(h => h.HashTag).ToListAsync();

            return bookmrkedContacts;
        }

        public async Task<ICollection<PhoneNumber>> GetPhoneNumbers(int id)
        {   
            return await _context.PhoneNumbers.Where(p => p.ContactId == id).ToListAsync();
        }

        public async Task<IEnumerable<Contact>> SearchContacts(SearchDto searchParams)
        {      
            if(searchParams.Type == null)
            {
                return null;
            } else if(searchParams.Query == null)
            {
                return null;
            }
             else if(searchParams.Type.Equals("First Name"))
            {
                return await _context.Contacts.Where(n => n.Name.ToLower().StartsWith(searchParams.Query.ToLower()))
                                .Include(p=>p.PhoneNumbers).Include(e=> e.Emails)
                                .Include(h => h.HashTags).ThenInclude(h => h.HashTag).ToListAsync();
            } else if(searchParams.Type.Equals("Last Name"))
            {
                return await _context.Contacts.Where(s => s.Surname.ToLower().StartsWith(searchParams.Query.ToLower()))
                                .Include(p=>p.PhoneNumbers).Include(e=> e.Emails)
                                .Include(h => h.HashTags).ThenInclude(h => h.HashTag).ToListAsync();
            } else if(searchParams.Type.Equals("Hashtag"))
            {
                return   await _context.Contacts
                        .Where(c => c.HashTags.Any(a => a.HashTag.HashName.ToLower().StartsWith(searchParams.Query.ToLower())))
                        .Include(n => n.PhoneNumbers).Include(e => e.Emails)
                        .Include(h => h.HashTags).ThenInclude(h => h.HashTag).ToListAsync();
            } else 
        
            {
                return null;
            }
        }

        public async Task<HashTag> GetHashTag(string hashName)
        {
           return await _context.HashTags.FirstOrDefaultAsync(h => h.HashName.Equals(hashName));
        }

        public async Task<IEnumerable<Contact>> GetContactsWithSameTag(int hashTagId)
        {
    
            return   await _context.Contacts.Where(c => c.HashTags.Any(a => a.HashTagId == hashTagId))
                        .Include(n => n.PhoneNumbers).Include(e => e.Emails)
                        .Include(h => h.HashTags).ThenInclude(h => h.HashTag).ToListAsync();
          
         }
    }
}