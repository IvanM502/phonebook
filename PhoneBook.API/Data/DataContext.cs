using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using PhoneBook.API.Models;

namespace PhoneBook.API.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base (options){}
        
         public DbSet<Contact> Contacts {get; set;} 

         public DbSet<PhoneNumber> PhoneNumbers {get; set;}
         public DbSet<HashTag> HashTags {get; set;}
        public DbSet<ContactHash> ContactHashTable {get; set;}

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<ContactHash>()
                .HasKey(k => new {k.ContactId, k.HashTagId});

            builder.Entity<ContactHash>()
                .HasOne(h => h.Contact)
                .WithMany(c => c.HashTags)
                .HasForeignKey(k => k.ContactId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.Entity<ContactHash>()
                .HasOne(h => h.HashTag)
                .WithMany(c => c.Contacts)
                .HasForeignKey(k => k.HashTagId)
                .OnDelete(DeleteBehavior.Cascade);
        }

         
    }
}