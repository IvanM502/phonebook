using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using PhoneBook.API.Dtos;
using PhoneBook.API.Models;

namespace PhoneBook.API.Data
{
    public class Seed
    {

        public static void SeedContacts(DataContext context)
        {
            if (!context.Contacts.Any())
            {
                var data = System.IO.File.ReadAllText("Data/ContactsSeedData.json");
                List<ContactDto> contacts = JsonConvert.DeserializeObject<List<ContactDto>>(data);


                foreach (var contact in contacts)
                {
                    var newContact = new Contact()
                    {
                        Name = contact.Name,
                        Surname = contact.Surname,
                        Adress = contact.Adress,
                        Bookmark = contact.Bookmark,
                        PhoneNumbers = contact.Numbers,
                        Emails = contact.ContactEmails
                    };
                    if (contact.ContactHashTags != null)
                    {

                        foreach (var hashTag in contact.ContactHashTags)
                        {
                            var tag = context.HashTags.FirstOrDefault(h => h.HashName.Equals(hashTag.HashName));
                            if (tag == null)
                            {
                                var newTag = new HashTag { HashName = hashTag.HashName };
                                context.HashTags.Add(newTag);
                                var contactHashTable = new ContactHash
                                {
                                    Contact = newContact,
                                    HashTag = newTag
                                };
                                context.ContactHashTable.Add(contactHashTable);
                            }
                            else
                            {
                                var contactHashTable = new ContactHash
                                {
                                    Contact = newContact,
                                    HashTagId = tag.HashTagId
                                };
                                context.ContactHashTable.Add(contactHashTable);
                            }

                        }
                    }

                    context.Contacts.Add(newContact);
                    context.SaveChanges();
                }


            }

        }
    }
}