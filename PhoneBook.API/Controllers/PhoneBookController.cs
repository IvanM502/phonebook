using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using PhoneBook.API.Data;
using PhoneBook.API.Dtos;
using PhoneBook.API.Models;

namespace PhoneBook.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PhoneBookController : ControllerBase
    {
        private readonly IPhoneBookRepository _repo;
        public PhoneBookController(IPhoneBookRepository repo)
        {
            _repo = repo;
        }

        [HttpGet]
        public async Task<IActionResult> GetContacts()
        {
            var contacts = await _repo.GetContacts();

            if (contacts == null)
                return NotFound();

            return Ok(contacts);
        }

        [HttpGet("{id}", Name = "GetUser")]
        public async Task<IActionResult> GetContact(int id)
        {
            var contact = await _repo.GetContact(id);

            return Ok(contact);
        }

        [HttpPost]
        public async Task<IActionResult> CreateContact(ContactDto contactToPostDto)
        {
            var newContact = new Contact
            {
                Name = contactToPostDto.Name,
                Surname = contactToPostDto.Surname,
                Adress = contactToPostDto.Adress,
                Bookmark = contactToPostDto.Bookmark,
                PhoneNumbers = contactToPostDto.Numbers,
                Emails = contactToPostDto.ContactEmails
            };

            if (contactToPostDto.ContactHashTags != null)
            {
                foreach (var hashTag in contactToPostDto.ContactHashTags)
                {
                    var tag = await _repo.GetHashTag(hashTag.HashName);
                    if (tag == null)
                    {
                        var newTag = new HashTag { HashName = hashTag.HashName };
                        var contactHashTable = new ContactHash
                        {
                            Contact = newContact,
                            HashTag = newTag
                        };
                        _repo.Add<ContactHash>(contactHashTable);
                    }
                    else
                    {
                         var contactHashTable = new ContactHash
                        {
                            Contact = newContact,
                            HashTagId = tag.HashTagId
                        };
                        _repo.Add<ContactHash>(contactHashTable);
                    }
                }
            }

            _repo.Add<Contact>(newContact);


            if (await _repo.SaveAll())
            {
                return CreatedAtRoute("GetUser", new { id = newContact.ContactId }, newContact);
            }

            throw new Exception("Creating new contact have failed.");
        }

        [HttpGet("bookmarks")]
        public async Task<IActionResult> GetBookmarkedContacts()
        {
            var bookmarkedContacts = await _repo.GetBookmarkedContacts();

            return Ok(bookmarkedContacts);

        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteContact(int id)
        {
            var contact = await _repo.GetContact(id);

            if (contact == null)
                return NotFound();

            _repo.Delete(contact);

            if (await _repo.SaveAll())
                return Ok();

            return BadRequest("Failed to delete contact");

        }

        [HttpPut("{id}/setbookmark")]
        public async Task<IActionResult> UpdateBookmark(int id)
        {
            var contact = await _repo.GetContact(id);

            if (contact == null)
                return NotFound();


            if (contact.Bookmark == 0) contact.Bookmark = 1;
            else if (contact.Bookmark == 1) contact.Bookmark = 0;

            if (await _repo.SaveAll())
                return NoContent();

            return BadRequest("Could not set bookmark");
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateContact(int id, ContactDto contactUpdate)
        {
            var contact = await _repo.GetContact(id);

            if (contact == null)
                return NotFound();

            contact.Name = contactUpdate.Name;
            contact.Surname = contactUpdate.Surname;
            contact.Adress = contactUpdate.Adress;
            contact.PhoneNumbers = contactUpdate.Numbers;
            contact.Emails = contactUpdate.ContactEmails;

            var contactHashTags = contact.HashTags;

            foreach(var tag in contactHashTags)
            {
                _repo.Delete(tag);
            }
            if (contactUpdate.ContactHashTags != null)
            {
                foreach (var hashTag in contactUpdate.ContactHashTags)
                {
                    var tag = await _repo.GetHashTag(hashTag.HashName);
                    if (tag == null)
                    {
                        var newTag = new HashTag { HashName = hashTag.HashName };
                        var contactHashTable = new ContactHash
                        {
                            ContactId = contact.ContactId,
                            HashTag = newTag
                        };
                        _repo.Add<ContactHash>(contactHashTable);
                    }
                    else
                    {
                         var contactHashTable = new ContactHash
                        {
                            ContactId = contact.ContactId,
                            HashTagId = tag.HashTagId
                        };
                        _repo.Add<ContactHash>(contactHashTable);
                    } 
                }
            }


            if (await _repo.SaveAll())
                return NoContent();

            return BadRequest("Could not update contact");

        }
        [HttpGet("search")]
        public async Task<IActionResult> SearchContacts([FromQuery] SearchDto searchParams)
        {
            var contacts = await _repo.SearchContacts(searchParams);

            return Ok(contacts);
        }

        [HttpGet("hashtag/{name}")]

        public async Task<IActionResult> GetContactsWithSameTag(string name)
        {
           var hashTag = await _repo.GetHashTag(name);
        
            if(hashTag == null)
                return NotFound(name);

            var contacts = await _repo.GetContactsWithSameTag(hashTag.HashTagId);
            

            return Ok(contacts);
        }
    }
}