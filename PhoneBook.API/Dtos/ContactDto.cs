using System.Collections.Generic;
using PhoneBook.API.Models;

namespace PhoneBook.API.Dtos
{
    public class ContactDto
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Adress { get; set; }
        public int Bookmark { get; set; }
        public ICollection<PhoneNumber> Numbers { get; set; }
         public ICollection<ContactEmail> ContactEmails { get; set; }
         public ICollection<HashTag> ContactHashTags { get; set; }
    
    }
}