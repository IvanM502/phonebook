namespace PhoneBook.API.Dtos
{
    public class SearchDto
    {
        public string Type { get; set; }
        public string Query { get; set; }
    }
}